# Slurm Puppet Module

## Overview

The [Slurm Workload Manager](http://slurm.schedmd.com/) is an open source project designed to manage compute
resources on computational clusters of various sizes.

## Forked

This module has been forked from [chwilk/puppet-slurm](https://github.com/chwilk/puppet-slurm).  There has been significant changes including adding code for supporting a slurm controller/server.

## Module Description

This module is intended for the automated provisioning and management of a SLURM based cluster.

For simplicity, some key configuration decisions have been assumed, namely:

* [MUNGE](https://code.google.com/p/munge/) will be used for shared key authentication, and the shared key can be provided to puppet via a URI.
* Standard MUNGE and SLURM packages have been compiled and made available to compute nodes via their installation repository of choice.
* slurm.conf is present on a shared file system, and that directory may be provided to the module as a parameter. /etc/slurm will be symlinked to it.

## Setup

### What slurm affects

* MUNGE packages will be installed (optional)
* The shared MUNGE key will be installed via exported resources.
* munge and slurmd services will be kept alive
* The PAM stack will be (optionally) edited to insert SLURM PAM controls to meter access to the node only to users running jobs on said node.
* Optionally, the [Warewulf Node Health Check](https://github.com/mej/nhc) will be installed and configured.

## Usage

The slurm module is intended to be modular for use in differently managed clusters. For instance:

We use something other than MUNGE to authenticate

```puppet
class slurm: {
    disable_munge => true,
    slurm_conf_location => '/shared/slurm/etc',
}

```

We don't want users logging into compute nodes whether or not they have jobs there.

```puppet
class slurm: {
    munge_key_filename => '/shared/secret/munge.key',
    slurm_conf_location => '/shared/slurm/etc',
    disable_pam => true,
}
```

## API

See [REFERENCE.md](REFERENCE.md)

## Limitations

This module is being developed on Red Hat Enterprise Linux (RHEL) version 6 and Ubuntu 16.04. Contributions helping to port to other distributions or operating systems are welcome.

## Development

Submit bugs and PRs via Bitbucket.
