# Slurm Puppet Module Changelog

## 2022/01/19 Version 6.0.0

- Updated to puppet 6 compliance.
- Removed Anchor pattern.
- Updated inline documentation to comply with puppet strings.

## 2016/10/27 Version 0.2.1

- Removed exported resources for munge as that did not work properly.
- Instead, added a content field for munge which will be deployed out to both head and nodes.

## 2016/10/26 Version 0.2.0

- Added a parameter to install a node or a server
- The slurm.conf is now managed by puppet and erb. The file is
  deployed to all nodes and server via the erb template.
- munge file is now an exported resource which is created on the 
  server (exported) and consumed on each node.
- updated formatting based on puppet-lint
- removed inherits from all classes except init
- changed using -> to using require statements (uniformity throughout code).
- added header comments for each class.

## 2016/02/05 Version 0.1.2

- Updated `disable_slurmd` parameter to `ensure_slurmd`

## 2016/02/03 Version 0.1.1

- Added `disable_slurmd` parameter

## 2016/02/01 Version 0.1.0

- Packaged and uploaded 0.1.0 release to Puppet Forge
