# @summary Manages the configuration files for slurm.
#
class slurm::config (
  $head_hostname      = $slurm::head_hostname,
  $cluster_name       = $slurm::cluster_name,
  $node_names         = $slurm::node_names,
  $cpus               = $slurm::cpus,
  $real_memory        = $slurm::real_memory,
  $sockets            = $slurm::sockets,
  $cores_per_socket   = $slurm::cores_per_socket,
  $threads_per_socket = $slurm::threads_per_socket,
  $threads_per_core   = $slurm::threads_per_core,
  $partition_name     = $slurm::partition_name,
){

  # MUNGE
  unless $slurm::disable_munge {
    if $slurm::force_munge {
      file { "${slurm::sysconfigdir}/${slurm::munge_service_name}":
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => "OPTIONS='--force'\n",
      }
    }

    file {$slurm::munge_key_filename:
      ensure  => file,
      owner   => 'munge',
      group   => 'munge',
      mode    => '0400',
      content => $slurm::munge_content,
    }
  }

  # PAM
  unless $slurm::disable_pam {
    case $::operatingsystem {
      'RedHat': {
        if $::operatingsystemmajrelease >= 7 { # sshd is going to password-auth in 7 and above.
          file_line { 'slurm_pam_password_auth_suff_access':
            path  => '/etc/pam.d/password-auth',
            line  => 'account  sufficient     pam_access.so',
            match => 'pam_access.so',
          }

          file_line { 'slurm_pam_password_auth_req_slurm':
            path  => '/etc/pam.d/password-auth',
            line  => 'account  required     pam_slurm.so',
            after => 'pam_access.so',
          }
        }
        file_line { 'slurm_pam_system_auth_suff_access':
          path  => '/etc/pam.d/system-auth-ac',
          line  => 'account  sufficient     pam_access.so',
          match => 'pam_access.so',
        }

        file_line { 'slurm_pam_system_auth_req_slurm':
          path  => '/etc/pam.d/system-auth-ac',
          line  => 'account  required     pam_slurm.so',
          after => 'pam_access.so',
        }
      }
      default : {
        # do nothing
      }
    }
  }

  # SLURM
  # configure slurm conf
  file{$slurm::slurm_conf_location:
    ensure  => file,
    content => template('slurm/slurm.conf.erb'),
  }

  file { "${slurm::sysconfigdir}/${slurm::slurm_service_name}":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => 'ulimit -l unlimited\n',
  }

  file {'/var/spool/slurm':
    ensure => directory,
    owner  => 'slurm',
    group  => 'slurm',
    mode   => '0755',
  }

  file {'/var/spool/slurm/logs':
    ensure => directory,
    owner  => 'slurm',
    group  => 'slurm',
    mode   => '0755',
  }

  file {'/var/run/slurm':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
}
