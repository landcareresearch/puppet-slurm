# @summary Manages the installation packages for slurm.
#
class slurm::install {

  if $slurm::package_manage {

    unless $slurm::disable_munge {
      # fix munge file permissions
      file{'/var/log':
        ensure => directory,
        mode   => '0755',
      }
      file{'/var/log/munge':
        ensure  => directory,
        mode    => '0755',
        require => File['/var/log'],
      }
      package {$slurm::munge_packages:
        ensure  => $slurm::package_ensure,
        require => File['/var/log/munge'],
      }
    }

    unless $slurm::disable_pam {
      package {$slurm::pam_packages:
        ensure  => $slurm::package_ensure,
      }
    }

    if $slurm::is_node {
      package { $slurm::slurm_packages:
        ensure  => $slurm::package_ensure,
      }
    }else{
      package { $slurm::slurmctl_packages:
        ensure  => $slurm::package_ensure,
      }
    }
  }
}
