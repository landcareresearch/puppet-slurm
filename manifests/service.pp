# @summary Manages the slurm and munge services.
#
class slurm::service {
  unless $slurm::disable_slurmd {
    if $slurm::is_node {
      service {$slurm::slurm_service_name:
        ensure     => running,
        enable     => true,
        hasstatus  => true,
        hasrestart => true,
      }
    }else{
      service {$slurm::slurmctl_service_name:
        ensure     => running,
        enable     => true,
        hasstatus  => true,
        hasrestart => true,
      }
    }
  }
  unless $slurm::disable_munge {
    service {'munge':
      ensure     => running,
      enable     => true,
      hasstatus  => true,
      hasrestart => true,
    }
  }
}
