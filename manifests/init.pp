# @summary This module is intended for the automated provisioning and management of a SLURM based cluster.
#
# @param head_hostname
#  The head's hostname.
#
# @param cluster_name
#   The name of the cluster in the slurm config.
#
# @param node_names
#   The names of the nodes in your cluster.
#   Example: 'linux[1-32]'
#
# @param cpus
#   Count of processors on each compute node.
#
# @param sockets
#   Number of physical processor sockets/chips on the node.
#
# @param cores_per_socket
#   Number of cores in a single physical processor socket.
#
# @param threads_per_socket
#   The number of threads per socket.
#
# @param threads_per_core
#   Number of logical threads in a single physical core.
#
# @param real_memory
#   Amount of real memory.
#
# @param is_node
#   If true, this is a slurm node, otherwise its a slurm ctrl / server.
#
# @param partition_name
#   Name of the one partition to be created
#
# @param munge_content
#   The content of the munge file.
#   Note, if munge is displayed, leave as undef.
#
class slurm (
  $cluster_name,
  $node_names,
  $cpus,
  $real_memory,
  $sockets,
  $cores_per_socket,
  $threads_per_socket,
  $threads_per_core,
  $head_hostname         = $::hostname,
  $partition_name        = 'defq',
  $is_node               = true,
  $disable_munge         = $slurm::params::disable_munge,
  $munge_content         = undef,
  $disable_pam           = $slurm::params::disable_pam,
  $disable_slurmd        = $slurm::params::disable_slurmd,
  $force_munge           = $slurm::params::force_munge,
  $package_ensure        = $slurm::params::package_ensure,
  $package_manage        = $slurm::params::package_manage,

  $munge_key_filename    = $slurm::params::munge_key_filename,
  $slurm_conf_location   = $slurm::params::slurm_conf_location,

  $slurm_service_name    = $slurm::params::slurm_service_name,
  $slurmctl_service_name = $slurm::params::slurmctl_service_name,
  $munge_service_name    = $slurm::params::munge_service_name,
  $sysconfigdir          = $slurm::params::sysconfigdir,

  $munge_packages        = $slurm::params::munge_packages,
  $pam_packages          = $slurm::params::pam_packages,
  $slurm_packages        = $slurm::params::slurm_packages,

) inherits slurm::params {
  validate_bool($disable_munge)
  validate_bool($disable_pam)
  validate_bool($disable_slurmd)
  validate_bool($package_manage)

  if($package_manage) {
    validate_string($package_ensure)
    validate_array($slurm_packages)
    unless($disable_pam) {
      validate_array($pam_packages)
    }
    unless($disable_munge) {
      validate_array($munge_packages)
    }
  }
  unless($disable_munge) {
    validate_string($munge_key_filename)
    validate_bool($force_munge)
    validate_string($munge_service_name)
  }
  validate_absolute_path($slurm_conf_location)
  validate_string($slurm_service_name)
  validate_absolute_path($sysconfigdir)

  contain slurm::install
  contain slurm::config
  contain slurm::service

  Class['slurm::install'] -> Class['slurm::config']
  Class['slurm::config']  -> Class['slurm::service']
}
